/*******************************************************************************
 * Copyright (C) 2010, Matthias Sohn <matthias.sohn@sap.com>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

/**
 * Binary plus operation
 */
public class Power extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		
		float inputOne = arg1;
		float inputTwo = arg2;
		float i;
		
		
		  float resultVal = 0;
		
		  float resultVal3 = 0;
		
	      if (inputOne == 0) {        // Because 0 to the power of anything is one,  this section will end this function and return the value of one
	         resultVal = 1;       
	      }
	      else {
	             
	              resultVal = arg1;
	              
	    	  for (i = inputTwo; i > 1; --i) {
	    		   
	    		
	    		  resultVal3 = resultVal * arg1; // This will take the last result of resultVal and multiply it by the original base
	    		 
		           resultVal = resultVal3; // This will save the most recent results of multiplying arg1 by itself
	    		  
	    		  
	    		  
	    	
	    		}  
	    	  
	    	  
	    	 
	       
	      }

		
		
		return resultVal;
	}

	@Override
	public String getName() {
		return "x^x";
	}

}
